
const options = {
    numberOfRows: 6,
    numberOfColumns: 7,
    cellSize: '40px',
    gridClasses: ['grid'],
    cellClasses: ['cell'],
    targetElement: document.body,
}
const grid = new Grid(options)

const cell = grid.findCellByIndex(2, 4)
    cell.colorMe("purple")

const array = grid.findNeighbors(cell)
array.forEach(cell=>cell.colorMe("orange"))
console.log(grid.findNeighbors(cell))


// const grid2 = new Grid({
//     numberOfRows: 12,
//     numberOfColumns: 7,
//     cellSize: '10px',
//     gridClasses: ['grid'],
//     cellClasses: ['cell'],
//     targetElement: document.body,
// })
// const grid3 = new Grid({
//     numberOfRows: 3,
//     numberOfColumns: 7,
//     cellSize: '10px',
//     gridClasses: ['grid'],
//     cellClasses: ['cell'],
//     targetElement: document.body,
// })

