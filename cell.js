class Cell {
    constructor(options) {
        this.rowIndex = options.rowIndex
        this.cellIndex = options.cellIndex
        this.size = options.size
        this.classes = options.classes
        this.rowElement = options.rowElement
        this.element = null

        this.createElement()
    }

    createElement() {
        this.element = document.createElement('div')
        console.log(this.classes)
        this.element.classList.add(...this.classes)
        this.element.dataset.rowIndex = this.rowIndex
        this.element.dataset.cellIndex = this.rowIndex
        this.element.cellInstance = this
        this.rowElement.appendChild(this.element)
        this.element.style.height = this.size
        this.element.style.width = this.size
    }

    colorMe(color){
        this.element.style.backgroundColor = color
        
    }



}




