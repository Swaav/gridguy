

class Grid {
    constructor(options) {
        this.numberOfRows = options.numberOfRows
        this.numberOfColumns = options.numberOfColumns || 7
        this.cellSize = options.cellSize
        this.gridClasses = options.gridClasses || ['grid']
        this.cellClasses = options.cellClasses || ['cell']
        this.targetElement = options.targetElement || document.body
        // console.log(this.targetElement)
        this.gridArray = []

        this.buildRows()
    }

    buildRows() {
        for (let rowIndex = 0; rowIndex < this.numberOfRows; rowIndex++) {
            const row = []
            const rowElement = document.createElement('div')
            rowElement.classList.add('row')
            rowElement.style.height = this.cellSize
            this.targetElement.appendChild(rowElement)
            this.gridArray.push(row)

            for (let cellIndex = 0; cellIndex < this.numberOfColumns; cellIndex++) {
                const cell = this.createCell({
                    rowIndex, 
                    cellIndex, 
                    rowElement,
                    size: this.cellSize, 
                    classes: this.cellClasses, 
                })
                row.push(cell)
            }
        }
    }
    createCell(options) {
        //override this in child classes

        return new Cell(options)
    }

    //methods
    
    findCellByIndex(rowIndex, cellIndex) {
        const row = this.gridArray[rowIndex]
        const cell = row[cellIndex]
        if (row) {
            
            
            const cell = row[cellIndex]
        }
        return cell ? cell : null
    }
    // console.log(findCellByIndex(4, 8))



    findNeighbors(cell){
        let bob = []
        bob.push (this.findCellByIndex(cell.rowIndex, cell.cellIndex +1))
        bob.push (this.findCellByIndex(cell.rowIndex, cell.cellIndex -1))
        bob.push (this.findCellByIndex(cell.rowIndex +1, cell.cellIndex))
        bob.push (this.findCellByIndex(cell.rowIndex -1, cell.cellIndex))

        return bob
    }
}



